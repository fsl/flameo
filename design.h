/*  design.h

    Mark Woolrich, Tim Behrens and Matthew Webster - FMRIB Image Analysis Group

    Copyright (C) 2009 University of Oxford  */

/*  CCOPYRIGHT  */

#if !defined(design_h)
#define design_h

#include <string>
#include <iostream>
#include <fstream>
#include <vector>
#include "armawrap/newmat.h"
#include "miscmaths/miscmaths.h"
#include "newimage/newimageall.h"
#include "gsa.h"


namespace Gs{

  class Design
    {
    public:

      // constructor
      Design() :
	nevs(0),
	ntpts(0),
	ngs(0),
	voxelwise_dm(false)
	{
	}

      // load design matrix in from file and set it up
      void setup(bool loadcontrasts = true);
      GSA::GeneralSpatialAbstractor<float> gIO;

      // getters
      const int getnevs() const { return nevs; }
      const int getntpts() const { return ntpts; }
      const int getngs() const { return ngs; }
      const int dof() const { return int(MISCMATHS::ols_dof(dm)); }
      void setvarcopedata(float val){varcopedata=val;}

     // returns full, global design matrix
      const NEWMAT::Matrix& getdm() const { return dm; }

      // returns voxelwise design matrix with any voxelwise zero evs removed
      NEWMAT::ReturnMatrix getdm(int x, int y, int z) const;

      // returns voxelwise design matrix for group g with any voxelwise zero evs removed
      NEWMAT::ReturnMatrix getdm(int x, int y, int z, int g) const;

      const NEWMAT::Matrix& getcovsplit() const { return covsplit; }

      NEWMAT::ReturnMatrix gettcontrast(const int p_num) const { NEWMAT::RowVector ret = tcontrasts.Row(p_num); ret.Release(); return ret; }

      const NEWMAT::Matrix& getfcontrast(const int p_num) const { return fc[p_num-1]; }

      int getgroup(int t) const { return int(group_index(t)); }
      int getglobalindex(int g, int within_t) const { return global_index[g-1][within_t-1]; }
      int getindexingroup(int g, int global_t) const { return index_in_group[g-1][global_t-1]; }

      int getntptsingroup(int g) const { return int(ntptsing(g)); }
      int getnevsingroup(int g) const { return int(nevsing(g)); }

      bool tcontrast_has_zeroevs(int x, int y, int z,const NEWMAT::RowVector& tcontrast)
      {
// 	cout<< "evs_group" << evs_group<< endl;
// 	cout<< "tcontrast" << tcontrast<< endl;
	bool in=false;
	for(int e=1; e<=tcontrast.Ncols() && !in; e++)
	  {
	    in = (tcontrast(e)!=0 && zero_evs(x,y,z,e-1));
	  }
	return in;
      }

      bool is_group_in_tcontrast(int g, const NEWMAT::RowVector& tcontrast)
      {
// 	cout<< "evs_group" << evs_group<< endl;
// 	cout<< "tcontrast" << tcontrast<< endl;
	bool in=false;
	for(int e=1; e<=tcontrast.Ncols() && !in; e++)
	  {
	    in = (tcontrast(e)!=0 && evs_group(e)==g);
	  }
	return in;
      }

     bool fcontrast_has_zeroevs(int x, int y, int z,const NEWMAT::Matrix& fcontrast)
      {
	bool in=false;
	for(int f=1; f<=fcontrast.Nrows() && !in; f++)
	  {
	    for(int e=1; e<=fcontrast.Ncols() && !in; e++)
	      {
		in = (fcontrast(f,e)!=0 && zero_evs(x,y,z,e-1));
	      }
	  }
	return in;
      }

      bool is_group_in_fcontrast(int g, const NEWMAT::Matrix& fcontrast)
      {
	bool in=false;
	for(int f=1; f<=fcontrast.Nrows() && !in; f++)
	  {
	    for(int e=1; e<=fcontrast.Ncols() && !in; e++)
	      {
		in = (fcontrast(f,e)!=0 && evs_group(e)==g);
	      }
	  }
	return in;
      }
      bool is_voxelwise_dm() const {return voxelwise_dm;}

      // indexed by ev number. Returns group that that ev belongs to
      int get_evs_group(int e, int x, int y, int z) {
	int ret = int(evs_group(e));
	if(zero_evs(x,y,z,e-1))
	  ret=-1;
	return ret;
      }

      int getnumfcontrasts() const { return numFcontrasts; }
      int getnumtcontrasts() const { return numTcontrasts; }
      //const Matrix& getfreduceddm(const int p_num) const { return reduceddms[p_num-1]; }

      const NEWIMAGE::volume4D<float>& getcopedata() const {return copedata;}
      const NEWIMAGE::volume4D<float>& getvarcopedata() const {return varcopedata;}
      const NEWIMAGE::volume4D<float>& getdofvarcopedata() const {return dofvarcopedata;}
      const NEWIMAGE::volume<float>& getsumdofvarcopedata() const {return sum_dofvarcopedata;}
      const NEWIMAGE::volume<float>& getmask() const {return mask;}

      NEWMAT::ReturnMatrix remove_zeroev_pes(int x, int y, int z, const NEWMAT::ColumnVector& petmp);
      NEWMAT::ReturnMatrix remove_zeroev_covpes(int x, int y, int z, const NEWMAT::SymmetricMatrix& covpetmp);
      NEWMAT::ReturnMatrix insert_zeroev_pemcmcsamples(int x, int y, int z, const NEWMAT::Matrix& mcmcin);
      NEWMAT::ReturnMatrix insert_zeroev_pes(int x, int y, int z, const NEWMAT::ColumnVector& petmp);
      NEWMAT::ReturnMatrix insert_zeroev_covpes(int x, int y, int z, const NEWMAT::SymmetricMatrix& covpetmp);

      NEWMAT::ReturnMatrix getcopedata(int x, int y, int z, int g);
      NEWMAT::ReturnMatrix getvarcopedata(int x, int y, int z, int g);

      void readFileAsVolume(const std::string& filename, NEWIMAGE::volume4D<float>& target, const NEWIMAGE::volume<float>& mask);
      void saveVolumeAsFile(const NEWIMAGE::volume<float>& source, const std::string& filename, const NEWIMAGE::volume<float>& mask);
    private:

      const Design& operator=(Design& par);
      Design(Design& des) { operator=(des); }

      void setupfcontrasts();

      int nevs;
      int ntpts;
      int ngs;

      NEWMAT::Matrix dm;
      NEWMAT::Matrix covsplit;

      std::vector<NEWIMAGE::volume4D<float> > voxelwise_evs;
      std::vector<int> voxelwise_ev_numbers;

      NEWMAT::Matrix tcontrasts;
      NEWMAT::Matrix fcontrasts;
      std::vector<NEWMAT::Matrix> fc;

      //      vector<Matrix> reduceddms;

      NEWMAT::ColumnVector group_index; // stores group for given global index
      std::vector<std::vector<int> > global_index; // stores global index for given group and within-group index

      NEWMAT::ColumnVector ntptsing;
      NEWMAT::ColumnVector nevsing;
      std::vector<std::vector<int> > index_in_group; // stores within-group index for given group and global index

      int numFcontrasts;
      int numTcontrasts;

      // indexed by ev number. Returns group that ev belongs to
      NEWMAT::ColumnVector evs_group;

      bool voxelwise_dm;

      // inputs
      NEWIMAGE::volume4D<float> copedata;
      NEWIMAGE::volume4D<float> varcopedata;
      NEWIMAGE::volume4D<float> dofvarcopedata;
      NEWIMAGE::volume<float> sum_dofvarcopedata;
      NEWIMAGE::volume<float> mask;

      NEWIMAGE::volume4D<int> zero_evs;

    };
}
#endif
