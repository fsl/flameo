/*  gsoptions.h

    Mark Woolrich, Tim Behrens - FMRIB Image Analysis Group

    Copyright (C) 2002 University of Oxford  */

/*  CCOPYRIGHT  */

#if !defined(GsOptions_h)
#define GsOptions_h

#include <string>
#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <stdio.h>
#include "utils/options.h"
#include "utils/log.h"

namespace Gs {

  class GsOptions {
  public:
    static GsOptions& getInstance();
    ~GsOptions() { delete gopt; }

    Utilities::Option<bool> verbose;
    Utilities::Option<int> debuglevel;
    Utilities::Option<bool> timingon;
    Utilities::Option<bool> help;
    Utilities::Option<std::string> copefile;
    Utilities::Option<std::string> varcopefile;
    Utilities::Option<std::string> dofvarcopefile;
    Utilities::Option<std::string> maskfile;
    Utilities::Option<std::string> designfile;
    Utilities::Option<std::string> covsplitfile;
    Utilities::Option<std::string> tcontrastsfile;
    Utilities::Option<std::string> fcontrastsfile;
    Utilities::Option<std::string> logdir;
    Utilities::Option<int> njumps;
    Utilities::Option<int> burnin;
    Utilities::Option<int> sampleevery;
    Utilities::Option<float> zlowerthreshold;
    Utilities::Option<float> zupperthreshold;
    Utilities::Option<bool> fixmeanfortfit;
    Utilities::Option<bool> infer_outliers;
    Utilities::Option<bool> no_pe_output;
    Utilities::Option<std::string> runmode;
    Utilities::Option<int> seed;
    Utilities::Option<std::vector<int> > voxelwise_ev_numbers;
    Utilities::Option<std::vector<std::string> > voxelwise_ev_filenames;
    Utilities::Option<float> sigma_smooth_flame2_dofs;
    Utilities::Option<float> sigma_smooth_globalproboutlier;
    Utilities::Option<int> io_niters;
    Utilities::Option<bool> outputDof;
    Utilities::Option<bool> readingCIFTI;

    void parse_command_line(int argc, char** argv, Utilities::Log& logger);

  private:
    GsOptions();
    const GsOptions& operator=(GsOptions&);
    GsOptions(GsOptions&);

    Utilities::OptionParser options;

    static GsOptions* gopt;

  };

  inline GsOptions& GsOptions::getInstance(){
    if(gopt == NULL)
      gopt = new GsOptions();

    return *gopt;
  }

  inline GsOptions::GsOptions() :
    verbose(std::string("-v,-V,--verbose"), false,
	    std::string("switch on diagnostic messages"),
	    false, Utilities::no_argument),
    debuglevel(std::string("--debug,--debuglevel"), 0,
	       std::string("set debug level"),
	       false, Utilities::requires_argument),
    timingon(std::string("--to,--timingon"), false,
	     std::string("turn timing on"),
	     false, Utilities::no_argument),
    help(std::string("-h,--help"), false,
	 std::string("display this message"),
	 false, Utilities::no_argument),
    copefile(std::string("--cope,--copefile"), std::string("cope"),
	     std::string("cope regressor data file"),
	     true, Utilities::requires_argument),
    varcopefile(std::string("--vc,--vcope,--varcope,--varcopefile"), std::string(""),
		std::string("varcope weightings data file"),
		false, Utilities::requires_argument),
    dofvarcopefile(std::string("--dvc,--dvcope,--dofvarcope,--dofvarcopefile"), std::string(""),
		   std::string("DOF data file for the varcope data"),
		   false, Utilities::requires_argument),
    maskfile(std::string("--mask,--maskfile"), std::string(""),
	     std::string("mask file"),
	     true, Utilities::requires_argument),
    designfile(std::string("--dm,--designfile"), std::string("design.mat"),
	       std::string("design matrix file"),
	       true, Utilities::requires_argument),
    covsplitfile(std::string("--cs,--csf,--covsplitfile"), std::string("design.gs"),
		 std::string("file containing an ASCII matrix specifying the groups the covariance is split into"),
		 true, Utilities::requires_argument),
    tcontrastsfile(std::string("--tc,--tcf,--tcontrastsfile"), std::string("design.con"),
		   std::string("file containing an ASCII matrix specifying the t contrasts"),
		   true, Utilities::requires_argument),
    fcontrastsfile(std::string("--fc,--fcf,--fcontrastsfile"), std::string(""),
		   std::string("file containing an ASCII matrix specifying the f contrasts"),
		   false, Utilities::requires_argument),
    logdir(std::string("--ld,--logdir"), std::string("logdir"),
	   std::string("log directory"),
	   false, Utilities::requires_argument),
    njumps(std::string("--nj,--njumps"), 5000,
	   std::string("Num of jumps to be made by MCMC"),
	   false, Utilities::requires_argument),
    burnin(std::string("--bi,--burnin"), 500,
	   std::string("Num of jumps at start of MCMC to be discarded"),
	   false, Utilities::requires_argument),
    sampleevery(std::string("--se,--sampleevery"), 1,
		std::string("Num of jumps for each sample"),
		false, Utilities::requires_argument),
    zlowerthreshold(std::string("--zlt"), 0,
		    std::string("Absolute value of lower threshold on FLAME stage 1 z-stats, used to see which voxels are passed to FLAME stage 2"),
		    false, Utilities::requires_argument),
    zupperthreshold(std::string("--zut"), 0,
		    std::string("Absolute value of upper threshold on FLAME stage 1 z-stats, used to see which voxels are passed to FLAME stage 2"),
		    false, Utilities::requires_argument),
    fixmeanfortfit(std::string("--fm,--fixmean"), false,
		   std::string("Fix mean for tfit"),
		   false, Utilities::no_argument),
    infer_outliers(std::string("--io,--inferoutliers"), false,
		   std::string("Infer outliers -- note that does not apply to fixed effects"),
		   false, Utilities::no_argument),
    no_pe_output(std::string("--npo,--nopeoutput"), false,
		   std::string("Do not output PE files"),
		   false, Utilities::no_argument),
    runmode(std::string("--runmode"), std::string(""),
	    std::string("Inference to perform: runmode=fe (fixed effects), runmode=ols (mixed effects - OLS), runmode=flame1 (mixed effects - FLAME stage 1), runmode=flame12 (mixed effects - FLAME stage 1+2)"),
	    true, Utilities::requires_argument),
    seed(std::string("--seed"), 10,
	 std::string("seed for pseudo random number generator"),
	 false, Utilities::requires_argument),
    voxelwise_ev_numbers(std::string("--voxelwise_ev_numbers,--ven"), std::vector<int>(),
	 std::string("List of numbers indicating voxelwise EVs position in the design matrix (list corresponds in order to files in voxelwise_ev_filenames)"),
	 false, Utilities::requires_argument),
    voxelwise_ev_filenames(std::string("--voxelwise_ev_filenames,--vef"), std::vector<std::string>(),
	 std::string("List of 4D niftii files containing voxelwise EVs (list corresponds in order to numbers in voxelwise_ev_numbers)"),
	 false, Utilities::requires_argument),
    sigma_smooth_flame2_dofs(std::string("--sdof,--sigma_dofs"), 1,
				   std::string("sigma (in mm) to use for Gaussian smoothing the DOFs in FLAME 2. Default is 1mm, -1 indicates no smoothing"),
	 false, Utilities::requires_argument),
    sigma_smooth_globalproboutlier(std::string("--so"), -1,
				   std::string(""),
	 false, Utilities::requires_argument),
    io_niters(std::string("--ioni"), 12,
				   std::string("Number of max iterations to use when inferring outliers. Default is 12."),
	 false, Utilities::requires_argument),
    outputDof(std::string("--outputdof"), false,
	    std::string("output dof for lower-level compatibility usage"),
	    false, Utilities::no_argument),
    readingCIFTI(std::string("--CIFTI"), false,
		 std::string("Treat input files as CIFTI - CAUTION: BETA option"),
		 false, Utilities::no_argument),
    options("flameo"," flameo --cope=filtered_func_data --mask=mask --dm=design.mat --tc=design.con --cs=design.grp --runmode=ols\n flameo --cope=filtered_func_data --varcope=var_filtered_func_data --mask=mask --dm=design.mat --tc=design.con --cs=design.grp --runmode=flame1")
  {
    try {
      options.add(verbose);
      options.add(debuglevel);
      options.add(timingon);
      options.add(help);
      options.add(copefile);
      options.add(varcopefile);
      options.add(dofvarcopefile);
      options.add(maskfile);
      options.add(designfile);
      options.add(tcontrastsfile);
      options.add(fcontrastsfile);
      options.add(covsplitfile);
      options.add(logdir);
      options.add(njumps);
      options.add(burnin);
      options.add(sampleevery);
      options.add(zlowerthreshold);
      options.add(zupperthreshold);
      options.add(fixmeanfortfit);
      options.add(infer_outliers);
      options.add(no_pe_output);
      options.add(runmode);
      options.add(seed);
      options.add(voxelwise_ev_numbers);
      options.add(voxelwise_ev_filenames);
      options.add(sigma_smooth_flame2_dofs);
      options.add(sigma_smooth_globalproboutlier);
      options.add(io_niters);
      options.add(outputDof);
      options.add(readingCIFTI);

    }
    catch(Utilities::X_OptionError& e) {
      options.usage();
      std::cerr << std::endl << e.what() << std::endl;
    }
    catch(std::exception &e) {
      std::cerr << e.what() << std::endl;
    }

  }
}

#endif
