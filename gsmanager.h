/*  gsmanager.h

    Mark Woolrich, Tim Behrens - FMRIB Image Analysis Group

    Copyright (C) 2002 University of Oxford  */

/*  CCOPYRIGHT  */

#if !defined(gsmanager_h)
#define gsmanager_h

#include <iostream>
#include <fstream>
#include <string>
#include <vector>

#include "armawrap/newmat.h"
#include "newimage/newimageall.h"
#include "miscmaths/miscmaths.h"
#include "gsoptions.h"
#include "design.h"


namespace Gs {

  // Give this class a file containing
  class Gsmanager
    {
    public:

      // constructor
      Gsmanager() :
	opts(GsOptions::getInstance()),
	nmaskvoxels(0)
	{
	}

      // load data from file in from file and set up starting values
      void setup();

      // initialise
      void initialise();

      void run();

      // saves results in logging directory
      void save();

      // Destructor
      virtual ~Gsmanager() {}

    private:

      const Gsmanager& operator=(Gsmanager& par);
      Gsmanager(Gsmanager& des);

      void multitfit(const NEWMAT::Matrix& x, NEWMAT::ColumnVector& m, NEWMAT::SymmetricMatrix& covar, float& v, bool fixmean=false) const;

      float log_likelihood(float beta, const NEWMAT::ColumnVector& gam, const NEWMAT::ColumnVector& y, const NEWMAT::Matrix& z, const NEWMAT::ColumnVector& S);
      float log_likelihood_outlier(float beta, float beta_outlier, const NEWMAT::ColumnVector& gam, const NEWMAT::ColumnVector& y, const NEWMAT::Matrix& z, const NEWMAT::ColumnVector& S, float global_prob_outlier, const NEWMAT::ColumnVector& prob_outlier);

      float marg_posterior_energy(float x, const NEWMAT::ColumnVector& y, const NEWMAT::Matrix& z, const NEWMAT::ColumnVector& S);
      float marg_posterior_energy_outlier(float logbeta, float logbeta_outlier, const NEWMAT::ColumnVector& y, const NEWMAT::Matrix& z, const NEWMAT::ColumnVector& S, const NEWMAT::ColumnVector& prob_outlier);

      float solveforbeta(const NEWMAT::ColumnVector& y, const NEWMAT::Matrix& z, const NEWMAT::ColumnVector& S);

      bool pass_through_to_mcmc(float zlowerthresh, float zupperthresh, int px, int py, int pz);

      // functions to calc contrasts
      void ols_contrasts(const NEWMAT::ColumnVector& gammean, const NEWMAT::SymmetricMatrix& gamS, int px, int py, int pz);
      void fe_contrasts(const NEWMAT::ColumnVector& gammean, const NEWMAT::SymmetricMatrix& gamS, int px, int py, int pz);
      void flame1_contrasts(const NEWMAT::ColumnVector& gammean, const NEWMAT::SymmetricMatrix& gamS, int px, int py, int pz);
      void flame1_contrasts_with_outliers(const NEWMAT::ColumnVector& mn, const NEWMAT::SymmetricMatrix& covariance, int px, int py, int pz);
      void flame2_contrasts(const NEWMAT::Matrix& gamsamples, int px, int py, int pz);

      void t_ols_contrast(const NEWMAT::ColumnVector& gammean, const NEWMAT::SymmetricMatrix& gamS, const NEWMAT::RowVector& tcontrast, float& cope, float& varcope, float& t, float& dof, float& z, int px, int py, int pz);
      void f_ols_contrast(const NEWMAT::ColumnVector& gammean, const NEWMAT::SymmetricMatrix& gamS, const NEWMAT::Matrix& fcontrast, float& f, float& dof1, float& dof2, float& z, int px, int py, int pz);


      void t_mcmc_contrast(const NEWMAT::Matrix& gamsamples, const NEWMAT::RowVector& tcontrast, float& cope, float& varcope, float& t, float& dof, float& z, int px, int py, int pz);
      void f_mcmc_contrast(const NEWMAT::Matrix& gamsamples, const NEWMAT::Matrix& fcontrast, float& f, float& dof1, float& dof2, float& z, int px, int py, int pz);


      // voxelwise functions to perform the different inference approaches
      void fixed_effects_onvoxel(const NEWMAT::ColumnVector& Y, const NEWMAT::Matrix& z, const NEWMAT::ColumnVector& S, NEWMAT::ColumnVector& gam, NEWMAT::SymmetricMatrix& gamcovariance);
      void flame_stage1_onvoxel(const std::vector<NEWMAT::ColumnVector>& Yg, const NEWMAT::ColumnVector& Y, const std::vector<NEWMAT::Matrix>& zg, const NEWMAT::Matrix& z, const std::vector<NEWMAT::ColumnVector>& Sg, const NEWMAT::ColumnVector& S, NEWMAT::ColumnVector& beta, NEWMAT::ColumnVector& gam, NEWMAT::SymmetricMatrix& gamcovariance, std::vector<float>& marg, std::vector<NEWMAT::ColumnVector>& weights_g, int& nparams, int px, int py, int pz);
      void flame_stage1_onvoxel_inferoutliers(const std::vector<NEWMAT::ColumnVector>& Yg, const NEWMAT::ColumnVector& Y, const std::vector<NEWMAT::Matrix>& zg, const NEWMAT::Matrix& z, const std::vector<NEWMAT::ColumnVector>& Sg, const NEWMAT::ColumnVector& S, NEWMAT::ColumnVector& beta, NEWMAT::ColumnVector& gam, NEWMAT::SymmetricMatrix& gamcovariance, NEWMAT::ColumnVector& global_prob_outlier, std::vector<NEWMAT::ColumnVector>& prob_outlier_g,  NEWMAT::ColumnVector& prob_outlier, NEWMAT::ColumnVector& beta_outlier, std::vector<float>& marg, std::vector<NEWMAT::ColumnVector>& weights_g, int& nparams, std::vector<bool>& no_outliers, int px, int py, int pz);
      void flame_stage1_inferoutliers();
      void init_flame_stage1_inferoutliers();

      // functions to perform the different inference approaches
      void fixed_effects();
      void ols();
      void flame_stage1();
      void flame_stage2();
      void do_kmeans(const NEWMAT::Matrix& data,std::vector<int>& z,const int k,NEWMAT::Matrix& means);
      void randomise(std::vector< std::pair<float,int> >& r);
      std::vector< std::pair<float,int> > randomise(const int n);

      void regularise_flame2_contrasts();

      NEWIMAGE::volume<float> mcmc_mask;

      // intermediates
      Design design;

      std::vector<NEWIMAGE::volume<float> > beta_b;
      std::vector<NEWIMAGE::volume<float> > beta_c;
      std::vector<NEWIMAGE::volume<float> > beta_mean;
      std::vector<NEWIMAGE::volume<float> > beta_outlier_mean;
      std::vector<NEWIMAGE::volume<float> > global_prob_outlier_mean;
      std::vector<NEWIMAGE::volume4D<float> > prob_outlier_mean;
      std::vector<NEWIMAGE::volume4D<float> > weights;

      NEWIMAGE::volume4D<float> cov_pes;

      // outputs

      std::vector<NEWIMAGE::volume<float> > pes;
      std::vector<NEWIMAGE::volume<float> > ts;
      std::vector<NEWIMAGE::volume<float> > tdofs;
      std::vector<NEWIMAGE::volume<float> > zts;
      std::vector<NEWIMAGE::volume<float> > zflame1upperts;
      std::vector<NEWIMAGE::volume<float> > zflame1lowerts;
      std::vector<NEWIMAGE::volume<float> > tcopes;
      std::vector<NEWIMAGE::volume<float> > tvarcopes;
      std::vector<NEWIMAGE::volume<float> > fs;
      std::vector<NEWIMAGE::volume<float> > fdof1s;
      std::vector<NEWIMAGE::volume<float> > fdof2s;
      std::vector<NEWIMAGE::volume<float> > zfs;
      std::vector<NEWIMAGE::volume<float> > zflame1upperfs;
      std::vector<NEWIMAGE::volume<float> > zflame1lowerfs;

      // intermediates
      int ngs;
      int nevs;
      int ntpts;
      int xsize;
      int ysize;
      int zsize;

      GsOptions& opts;

      int nmaskvoxels;

      bool dofpassedin;
    };

  bool compare(const std::pair<float,int> &r1,const std::pair<float,int> &r2);

}
#endif
