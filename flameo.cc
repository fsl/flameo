/*  FLAME - FMRIB's Local Analysis of Mixed Effects

    Mark Woolrich, Tim Behrens - FMRIB Image Analysis Group

    Copyright (C) 2002 University of Oxford  */

/*  CCOPYRIGHT  */

#include <iostream>
#include <fstream>
#include <iomanip>
#include <sstream>
#define WANT_STREAM
#define WANT_MATH
#include "armawrap/newmat.h"
#include <string>
#include <math.h>
#include "utils/log.h"
#include "gsmanager.h"
#include "gsoptions.h"
#include "utils/tracer_plus.h"
#include "miscmaths/miscprob.h"
#include "stdlib.h"

using namespace Utilities;
using namespace NEWMAT;
using namespace Gs;
using namespace MISCMATHS;
using namespace std;

int main(int argc, char *argv[])
{
  try{
    // Setup logging:
    Log& logger = LogSingleton::getInstance();

    // parse command line - will output arguments to logfile
    GsOptions& opts = GsOptions::getInstance();
    opts.parse_command_line(argc, argv, logger);

    srand(GsOptions::getInstance().seed.value());

    if(opts.debuglevel.value()==1)
      Tracer_Plus::setrunningstackon();

    if(opts.timingon.value())
      Tracer_Plus::settimingon();


    //     ColumnVector storen(10000);
    //     ColumnVector store(10000);
    //     for(int i=1; i<=10000; i++)
    //       {
    // 	storen(i) = normrnd().AsScalar();
    // 	store(i) = (unifrnd().AsScalar()+0.5);
    //       }

    //     write_ascii_matrix(storen,"storen");
    //     write_ascii_matrix(store,"store");

    Gsmanager gsmanager;
    cout << "Setting up:" << endl;
    gsmanager.setup();
    cout << "Running:" << endl;
    gsmanager.run();
    cout << "Saving results" << endl;
    gsmanager.save();

    if(opts.timingon.value())
      Tracer_Plus::dump_times(logger.getDir());

    cout << endl << "Log directory was: " << logger.getDir() << endl;
  }
  catch(Exception& e)
  {
    cerr << endl << e.what() << endl;
    return 1;
  }
  catch(X_OptionError& e)
  {
    cerr << endl << e.what() << endl;
    return 1;
  }

  return 0;
}
