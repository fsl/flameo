/*  design.cc

    Mark Woolrich, Tim Behrens - FMRIB Image Analysis Group

    Copyright (C) 2002 University of Oxford  */

/*  CCOPYRIGHT  */

#include "design.h"
#include "utils/log.h"
#include "miscmaths/miscmaths.h"
#include "armawrap/newmat.h"
#include "utils/tracer_plus.h"
#include "gsoptions.h"
#include <string>
#include "newimage/newimageall.h"

using namespace Utilities;
using namespace MISCMATHS;
using namespace NEWMAT;
using namespace NEWIMAGE;
using namespace std;

namespace Gs {

  ReturnMatrix Design::getdm(int x, int y, int z, int g) const {

    Tracer_Plus trace("Design::getdm x y z g");

    Matrix pdm = getdm(x,y,z);

    Matrix zg(getntptsingroup(g),pdm.Ncols());
    zg=0;

    int t2=1;
    for(int t = 1; t <= ntpts; t++)
    {
      if(getgroup(t)==g)
	  {
	    zg.Row(t2++) = pdm.Row(t);
	  }
    }

    // remove non-zero columns
    Matrix tmp=zg;
    int e2=1;
    for(int e = 1; e<= tmp.Ncols(); e++)
    {
      if(abs(tmp.Column(e)).Sum()>0)
        zg.Column(e2++) = tmp.Column(e);
    }

    zg=zg.Columns(1,e2-1);

    zg.Release();
    return zg;
  }

  // returns design matrix with any voxelwise zero evs removed
  ReturnMatrix Design::getdm(int x, int y, int z) const {

    Tracer_Plus trace("Design::getdm x y z");

    Matrix ret=dm;

    if(is_voxelwise_dm())
    {
      // insert voxelwise evs
      for(unsigned int i=0; i<voxelwise_ev_numbers.size(); i++)
	  {
	    ColumnVector ev_i=voxelwise_evs[i].voxelts(x,y,z);
	    ret.Column(voxelwise_ev_numbers[i])=ev_i;
	  }

      // remove any zero evs
      ColumnVector zero_ev=zero_evs.voxelts(x,y,z);
      if(zero_ev.Sum()>0)
	  {
	    int index=1;
	    Matrix new_ret=ret;
	    for(int i=1; i<=dm.Ncols(); i++)
        {
          if(zero_ev(i)==0)
		  {
		    new_ret.Column(index)=ret.Column(i);
		    index++;
		  }
        }

	    ret=new_ret.Columns(1,index-1);
	  }
    }

    ret.Release();
    return ret;
  }

  ReturnMatrix Design::remove_zeroev_pes(int x, int y, int z, const ColumnVector& petmp)
  {
    ColumnVector ret=petmp;

    ColumnVector zero_ev=zero_evs.voxelts(x,y,z);

    if(zero_ev.Sum()>0)
    {
      int index=1;
      for(int i=1; i<=nevs; i++)
	  {
	    if(zero_ev(i)==0)
        {
          ret(index)=petmp(i);
          index++;
        }
	  }

      ret=ret.Rows(1,index-1);
    }

    ret.Release();
    return ret;
  }

  ReturnMatrix Design::remove_zeroev_covpes(int x, int y, int z, const SymmetricMatrix& covpetmp)
  {
    SymmetricMatrix ret=covpetmp;

    ColumnVector zero_ev=zero_evs.voxelts(x,y,z);

    if(zero_ev.Sum()>0)
    {
      int index=1;
      for(int i=1; i<=nevs; i++)
	  {
	    int index2=i+1;
	    for(int j=i+1; j<=nevs; j++)
        {
          if(zero_ev(i)==0 && zero_ev(j)==0)
		  {
		    ret(index,index2)=covpetmp(i,j);
		    ret(index2,index)=covpetmp(j,i);
		  }

          if(zero_ev(j)==0)
		  {
		    index2++;
		  }
        }

	    if(zero_ev(i)==0)
        {
          ret(index,index)=covpetmp(i,i);
          index++;
        }
	    else
        {
          ret(index,index)=1e32; // set variance very high for this parameter as it is a zero ev
        }
	  }

      ret=ret.SymSubMatrix(1,index-1);
    }

    ret.Release();
    return ret;
  }

  ReturnMatrix Design::insert_zeroev_pemcmcsamples(int x, int y, int z, const Matrix& mcmcin)
  {
    Matrix ret=mcmcin;

    ColumnVector zero_ev=zero_evs.voxelts(x,y,z);

    if(zero_ev.Sum()>0)
    {
      ret.ReSize(nevs,mcmcin.Ncols());
      ret=0;
      int index=1;
      for(int i=1; i<=nevs; i++)
	  {
	    if(zero_ev(i)==0)
        {
          ret.Row(i)=mcmcin.Row(index);
          index++;
        }
	  }
    }

    ret.Release();
    return ret;
  }

  ReturnMatrix Design::insert_zeroev_pes(int x, int y, int z, const ColumnVector& petmp)
  {
    ColumnVector ret=petmp;

    ColumnVector zero_ev=zero_evs.voxelts(x,y,z);

    if(zero_ev.Sum()>0)
    {

      ret.ReSize(nevs);
      ret=0;
      int index=1;
      for(int i=1; i<=nevs; i++)
	  {
	    if(zero_ev(i)==0)
        {
          ret(i)=petmp(index);
          index++;
        }
	  }
    }

    ret.Release();
    return ret;
  }

  ReturnMatrix Design::insert_zeroev_covpes(int x, int y, int z, const SymmetricMatrix& covpetmp)
  {
    SymmetricMatrix ret=covpetmp;

    ColumnVector zero_ev=zero_evs.voxelts(x,y,z);

    if(zero_ev.Sum()>0) {
      ret.ReSize(nevs);
      ret=0;
      vector<int> realIndex(nevs+1,-1);
      int index(1);
      for(int i=1; i<=nevs; i++)
        if (zero_ev(i) == 0)
          realIndex.at(i)=index++;

      for(int i=1; i<=nevs; i++) {
        for(int j=i; j<=nevs; j++) {
          if ( realIndex.at(i) > 0 && realIndex.at(j) > 0 )
            ret(i,j)=covpetmp(realIndex.at(i),realIndex.at(j));
        }
        if ( realIndex.at(i) ==- 1 )
          ret(i,i) = 1e32; // set variance very high for this parameter as it is a zero ev
      }
    }
    ret.Release();
    return ret;
  }

  ReturnMatrix Design::getcopedata(int x, int y, int z, int g){
    ColumnVector Yg(getntptsingroup(g));
    Yg = 0;
    int t2=1;
    for(int t = 1; t <= ntpts; t++)
    {
      if(getgroup(t)==g)
	  {
	    Yg(t2) = copedata(x,y,z,t-1);
	    t2++;
	  }
    }

    Yg.Release();
    return Yg;
  }

  ReturnMatrix Design::getvarcopedata(int x, int y, int z, int g){
    ColumnVector Sg(getntptsingroup(g));
    Sg = 0;
    int t2=1;
    for(int t = 1; t <= ntpts; t++)
    {
      if(getgroup(t)==g)
	  {
	    Sg(t2) = varcopedata(x,y,z,t-1);
	    t2++;
	  }
    }

    Sg.Release();
    return Sg;
  }

  void Design::readFileAsVolume(const string& filename, volume4D<float>& target, const volume<float>& mask) {
    if(GsOptions::getInstance().readingCIFTI.value()) {
      Matrix tmpData(gIO.abstractFile(filename,"CIFTI"));
      target.setmatrix(tmpData,mask);
    } else {
      read_volume4D(target,filename);
    }

  }

  void Design::saveVolumeAsFile(const volume<float>& source, const string& filename, const volume<float>& mask) {
    if(GsOptions::getInstance().readingCIFTI.value()) {
      Matrix tmpData=source.matrix(mask);
      gIO.saveMatrix(tmpData,filename);
    } else {
      save_volume(source,filename);
    }
  }

  void Design::setup(bool loadcontrasts)
  {
    Tracer_Plus trace("Design::setup");

    // read data
    if(GsOptions::getInstance().readingCIFTI.value()) {
      GsOptions::getInstance().sigma_smooth_flame2_dofs.set_value("-1");
      Matrix tmpData=gIO.abstractFile(GsOptions::getInstance().copefile.value(),"CIFTI");
      cerr << "Rows/Cols " << tmpData.Ncols() << " " << tmpData.Nrows() << endl;
      short cubeDim=ceil(cbrt(tmpData.Ncols()));
      cout << "cubedim " << cubeDim << endl;
      mask.reinitialize(cubeDim,cubeDim,cubeDim);
      mask=0;
      int currentColumn(1);
      for(int z=0;z<mask.zsize();z++)
        for(int y=0;y<mask.ysize();y++)
          for(int x=0;x<mask.xsize();x++)
            if(currentColumn++<=tmpData.Ncols())
              mask(x,y,z)=1;
      copedata.setmatrix(tmpData,mask);
      save_volume4D(copedata,"test");
    } else {
      read_volume4D(copedata, GsOptions::getInstance().copefile.value());
      read_volume(mask, GsOptions::getInstance().maskfile.value());
    }


    if(GsOptions::getInstance().varcopefile.value() != string(""))
      readFileAsVolume(GsOptions::getInstance().varcopefile.value(), varcopedata, mask);
    else
    {
      // set to zero
      varcopedata=copedata;
      varcopedata=0;
    }

    if(GsOptions::getInstance().dofvarcopefile.value() != string(""))
    {
      readFileAsVolume(GsOptions::getInstance().dofvarcopefile.value(), dofvarcopedata, mask);
    }
    else
    {
      dofvarcopedata = varcopedata;
      dofvarcopedata = 0;
    }

    // create sum_dofvarcopedata
    sum_dofvarcopedata=dofvarcopedata[0];
    for(int i=1; i<=dofvarcopedata.maxt(); i++)
    {
      sum_dofvarcopedata+=dofvarcopedata[i];
    }

    dm = read_vest(GsOptions::getInstance().designfile.value());
    //write_ascii_matrix(dm,"dm");
    nevs = dm.Ncols();
    ntpts = dm.Nrows();

    // check design and data compatability
    if(getntpts() != copedata.tsize())
    {
      cout << "dm.getntpts()=" << getntpts() << endl;
      cout << "copedata.tsize()=" << copedata.tsize() << endl;
      throw Exception("Cope data and design have different numbers of time points");
    }
    if(getntpts() != varcopedata.tsize())
    {
      cout << "dm.ntpts()=" << getntpts() << endl;
      cout << "copedata.tsize()=" << varcopedata.tsize() << endl;

      throw Exception("Varcope data and design have different numbers of time points");
    }


    // read in variance groupings
    group_index = read_vest(GsOptions::getInstance().covsplitfile.value());
    nevs = dm.Ncols();
    ntpts = dm.Nrows();
    ngs = int(group_index.Maximum());

    if(nevs == 0 || ntpts == 0)
      throw Exception(string(GsOptions::getInstance().designfile.value() + " is an invalid design matrix file").data());

    if(ngs == 0 || group_index.Nrows() == 0)
      throw Exception(string(GsOptions::getInstance().covsplitfile.value() + " is an invalid covariance split file").data());

    if(ntpts != group_index.Nrows())
    {
      cerr << "design matrix has ntpts=" <<  ntpts << endl;
      cerr << "cov split file has ntpts=" << group_index.Nrows() << endl;
      throw Exception("The ntpts needs to be the same for both");
    }

    if(GsOptions::getInstance().debuglevel.value()==2)
    {
      cout << "******************************************" << endl
           << "Data Read Complete" << endl << "******************************************"
           << endl;
    }

    // fill group_index:
    covsplit.ReSize(ntpts,ngs);
    covsplit = 0;
    ntptsing.ReSize(ngs);
    ntptsing = 0;
    nevsing.ReSize(ngs);
    nevsing = 0;
    global_index.resize(ngs);
    index_in_group.resize(ngs);

    for(int t = 1; t <= ntpts; t++)
    {
      covsplit(t,int(group_index(t)))=1;
      ntptsing(int(group_index(t)))++;
      global_index[int(group_index(t))-1].push_back(t);
    }


    for(int g =1; g<=ngs; g++)
    {

      index_in_group[g-1].resize(ntpts);

      for(int wt = 1; wt <= int(ntptsing(g)); wt++)
	  {
	    int gt=global_index[g-1][wt-1];

	    index_in_group[g-1][gt-1]=wt;
	  }
    }


    cout << "ntptsing=" << ntptsing << endl;
    //     cout << "group_index=" << group_index << endl;

    // assign each EV to a group
    evs_group.ReSize(nevs);
    evs_group=0;
    for(int e=1; e <=nevs; e++)
    {
      for(int t = 1; t <= ntpts; t++)
	  {
	    if(dm(t,e)!=0)
        {
          if(evs_group(e)==0)
		  {
		    evs_group(e) = group_index(t);
		    nevsing(int(group_index(t)))++;
		  }
          else if(evs_group(e)!=group_index(t))
		  {
		    cerr << "nonseparable design matrix and variance groups" << endl;
		    throw Exception("nonseparable design matrix and variance groups");
		  }
        }
	  }
    }

    std::cout << evs_group << std::endl;

    // load contrasts:
    if(loadcontrasts)
    {
      try
	  {
	    tcontrasts = read_vest(GsOptions::getInstance().tcontrastsfile.value());

	    numTcontrasts = tcontrasts.Nrows();
	  }
      catch(Exception exp)
	  {
	    numTcontrasts = 0;
	  }

      // Check contrast matches design matrix
      if(numTcontrasts > 0 && dm.Ncols() != tcontrasts.Ncols())
	  {
	    cerr << "Num tcontrast cols  = " << tcontrasts.Ncols() << ", design matrix cols = " << dm.Ncols() << endl;
	    throw Exception("size of design matrix does not match t contrasts\n");
	  }

      try
	  {
	    fcontrasts = read_vest(GsOptions::getInstance().fcontrastsfile.value());
	    numFcontrasts = fcontrasts.Nrows();
	  }
      catch(Exception exp)
	  {
	    numFcontrasts = 0;
		cout << "No f contrasts" << endl;
	  }

      if(numFcontrasts > 0)
	  {
	    // Check contrasts match
	    if(tcontrasts.Nrows() != fcontrasts.Ncols())
        {
          cerr << "tcontrasts.Nrows()  = " << tcontrasts.Nrows() << endl;
          cerr << "fcontrasts.Ncols()  = " << fcontrasts.Ncols() << endl;
          throw Exception("size of tcontrasts does not match fcontrasts\n");
        }

	    if(numFcontrasts > 0)
	      setupfcontrasts();
	  }
    }

    // read in any voxelwise EVs
    voxelwise_ev_numbers=GsOptions::getInstance().voxelwise_ev_numbers.value();
    vector<string> voxelwise_ev_filenames=GsOptions::getInstance().voxelwise_ev_filenames.value();

    if(voxelwise_ev_filenames.size() != voxelwise_ev_numbers.size())
      throw Exception("Number of filenames in voxelwise_ev_filenames command line option needs to be the same as the number of EV number in the voxelwise_ev_numbers command line option");

    voxelwise_evs.resize(voxelwise_ev_filenames.size());
    voxelwise_dm=false;

    for(unsigned int i=0; i<voxelwise_ev_filenames.size(); i++)
    {
      if(voxelwise_ev_numbers[i]>nevs)
        throw Exception("EV number in the voxelwise_ev_numbers command line option is invalid (it's greater than the number of design matrix EVs)");

      voxelwise_dm=true;
      //	voxelwise_evs[i].read(voxelwise_ev_filenames[i]);
      read_volume4D(voxelwise_evs[i], voxelwise_ev_filenames[i]);
    }

    // find if there are any zero voxelwise evs
    // reinforce mask by checking for zeros in lower-level varcopes
    zero_evs.reinitialize(mask.xsize(),mask.ysize(),mask.zsize(),nevs);
    zero_evs=0;

    bool global_contains_zero_varcope=false;

    for(int x = 0; x < mask.xsize(); x++)
      for(int y = 0; y < mask.ysize(); y++)
        for(int z = 0; z < mask.zsize(); z++)
        {
          if(mask(x,y,z))
	      {
            // reinforce mask by checking for zeros in lower-level varcopes
            if(GsOptions::getInstance().varcopefile.value() != string(""))
            {
              bool contains_zero_varcope=false;
              for(int t = 1; t <= ntpts && !contains_zero_varcope; t++)
		      {
                if(varcopedata(x,y,z,t-1)<=0)
                  contains_zero_varcope=true;
		      }
              if(contains_zero_varcope)
		      {
                mask(x,y,z)=0; global_contains_zero_varcope=true;
		      }
            }

            // find if there are any zero voxelwise evs
            if(mask(x,y,z))
            {
              for(unsigned int i=0; i<voxelwise_ev_numbers.size(); i++)
		      {
                ColumnVector ev_i=voxelwise_evs[i].voxelts(x,y,z);

                if(abs(ev_i).Sum()==0)
                {
                  zero_evs(x,y,z,voxelwise_ev_numbers[i]-1)=1;
                }
		      }
            }
	      }
        }

    if(global_contains_zero_varcope)
    {
      cout << endl << "WARNING: The passed in varcope file, "<< GsOptions::getInstance().varcopefile.value()<< ", contains voxels inside the mask with zero (or negative) values. These voxels will be excluded from the analysis." << endl;
    }

    //save_volume4D(zero_evs, LogSingleton::getInstance().appendDir("zero_evs"));

  }

  void Design::setupfcontrasts()
  {
    Tracer_Plus trace("Design::setupfcontrasts");

    fc.resize(numFcontrasts);
    //reduceddms.resize(numFcontrasts);

    for(int f=0; f < numFcontrasts; f++)
    {
      fc[f].ReSize(tcontrasts.Nrows(),nevs);
      fc[f] = 0;

      int count = 0;

      for(int c = 1; c <= tcontrasts.Nrows(); c++)
      {
        if(fcontrasts(f+1,c) == 1)
	    {
	      count++;
	      fc[f].Row(count) = tcontrasts.Row(c);

	    }
      }

      fc[f] = fc[f].Rows(1,count);
    }
  }


}
