/*  Copyright (c) 2016 University of Oxford  */
/*  CCOPYRIGHT  */

#include "CiftiLib/CiftiFile.h"
#include "armawrap/newmat.h"
#include "newimage/newimageall.h"

#include "gsa.h"

using namespace std;
using namespace cifti;
using namespace NEWMAT;
using namespace NEWIMAGE;

namespace GSA {

  template<class T>
  ReturnMatrix GeneralSpatialAbstractor<T>::abstractFile(const string&filename,const string& mode) {
    Matrix tmpData;
    sourceType=mode;
    if ( sourceType.compare("CIFTI") == 0 ) {
      cifti::CiftiFile inputCifti;
      inputCifti.openFile(make_basename(filename)+".nii");
      ciftiExemplar=inputCifti.getCiftiXML();
      cerr << "ndim " << ciftiExemplar.getNumberOfDimensions() << endl;
      cerr << "type1 " << ciftiExemplar.getMappingType(0) << endl;
      cerr << "type2 " << ciftiExemplar.getMappingType(1) << endl;
      CiftiBrainModelsMap spatialModels(ciftiExemplar.getBrainModelsMap(1));
      std::vector<CiftiBrainModelsMap::ModelInfo> models(spatialModels.getModelInfo());
      std::vector<StructureEnum::Enum> nSurfaces(spatialModels.getSurfaceStructureList());
      cerr << "Num surfaces " << nSurfaces.size() << endl;
      cerr << "Has volume " << spatialModels.hasVolumeData() << endl;
      //for(unsigned int currentModel=0;currentModel<models.size();currentModel++)
      //cerr << StructureEnum::toName(models[currentModel].m_structure) << endl;
      const vector<int64_t>& dims = inputCifti.getDimensions();
      tmpData.ReSize(dims[0],dims[1]); //swapped compared to cifti
      vector<float> scratchRow(dims[0]);//read/write a row at a time
      for (int64_t row=0;row<dims[1];row++) {
	inputCifti.getRow(scratchRow.data(),row);
	for (int64_t col=0;col<dims[0];col++)
	  tmpData(col+1,row+1)=scratchRow[col];
      }
    }
    if ( sourceType.compare("NIFTI") == 0 ) {
      volume4D<T> inputNifti;
      NEWIMAGE::read_volume4D(inputNifti,filename);
      niftiGeometry=inputNifti[0];
      storageMap.resize(niftiGeometry.xsize()*niftiGeometry.ysize()*niftiGeometry.zsize()+1);
      list<uniqueNode> allVoxels;
      int64_t index(1);
      for (int64_t z=0; z<niftiGeometry.zsize(); z++)
	for (int64_t y=0; y<niftiGeometry.ysize(); y++)
	  for (int64_t x=0; x<niftiGeometry.xsize(); x++) {
	    storageMap[index].key.push_back(x);
	    storageMap[index].key.push_back(y);
	    storageMap[index].key.push_back(z);
	    storageMap[index].ID=index;
	    allVoxels.push_back(storageMap[index++]);
	}
      //VolumeGeometry<T> *myGeometry=new VolumeGeometry<T>(niftiGeometry,storageMap,allVoxels);
      //tmpData.ReSize(inputNifti.tsize(),myGeometry->nNodes());
      //myGeometry->updateMatrix(inputNifti,tmpData);
    }
    tmpData.Release();
    return tmpData;
}


  template<class T>
  void VolumeGeometry<T>::maskMap(const NEWIMAGE::volume<T>& mask) {
    //Remove any known nodes which are outside of mask and tag in storageMap
    for (std::list<uniqueNode>::iterator iNode=this->myNodes.begin();iNode!=this->myNodes.end();iNode++)
      if (mask(iNode->key[0],iNode->key[1],iNode->key[2])<mask.maskThreshold()) {
	this->dataMap[iNode->ID].ID=-1;
	iNode=this->myNodes.erase(iNode);
      }
  }

  template<class T>
  ReturnMatrix VolumeGeometry<T>::updateMapAndMatrix(const NEWIMAGE::volume<T>& mask, const Matrix& data) {
    volume4D<T> before=asVolume(data);
    updateMap(mask);
  }

  template<class T>
  NEWIMAGE::volume4D<T> VolumeGeometry<T>::asVolume(const Matrix& data) {
    volume4D<T> output(geometry.xsize(),geometry.ysize(),geometry.zsize(),data.Nrows());
    output.copyproperties(geometry);
    output=0;
    for (std::list<uniqueNode>::iterator iMap=this->dataMap.begin();iMap!=this->dataMap.end();iMap++)
      if (this->dataMap[iMap->ID]>0)
	for(int64_t t=0;t<data.Nrows();t++)
	  output(iMap->key[0],iMap->key[1],iMap->key[2],t)=data(t+1,this->dataMap[iMap->ID]);
  }


  template<class T>
  void VolumeGeometry<T>::updateMatrix(const NEWIMAGE::volume4D<T>& source, NEWMAT::Matrix& target) {
    for (std::list<uniqueNode>::iterator iMap=this->myNodes.begin();iMap!=this->myNodes.end();iMap++)
      if (this->dataMap[iMap->ID]>0)
	for(int64_t t=0;t<source.tsize();t++)
	  target(t+1,this->dataMap[iMap->ID])=source(iMap->key[0],iMap->key[1],iMap->key[2],t);
  }

  template<class T>
  void GeneralSpatialAbstractor<T>::remapData(Matrix& data) {
    //remap data(after masking)
    int nValid(0);
    for(vector<uniqueNode>::iterator iNode=storageMap.begin();iNode!=storageMap.end();iNode++)
      if(iNode->ID>0)
	nValid++;
    if(nValid==data.Ncols()) //No masking has occurred do not modify data
      return;
    Matrix newData(data.Nrows(),nValid);
    int newColumn(1);
    int oldColumn(1);
    for(vector<uniqueNode>::iterator iNode=storageMap.begin();iNode!=storageMap.end();iNode++) {
      if(iNode->ID>0) //Copy
	newData.Column(newColumn++)=data.Column(oldColumn++);
      else if (iNode->ID==-1) {//skip next Column in old data
	oldColumn++;
	iNode->ID=0;
      }
    }
    data=newData;
  }

  template<class T>
  void GeneralSpatialAbstractor<T>::saveMatrix(const Matrix& data, const string& filename) {
    if ( sourceType.compare("CIFTI") == 0 ) {
      char intentNameOut[16];
      int intent=ciftiExemplar.getIntentInfo(ciftiExemplar.getParsedVersion(),intentNameOut);
      string extension("");
      CiftiMappingType::MappingType type=ciftiExemplar.getMappingType(0);
      if ( intent == 3001 || intent == 3002 )
	extension=".dscalar";
      if ( intent == 3003 || intent == 3004 )
	extension=".pscalar";
      cifti::CiftiScalarsMap scalarsMap;
      scalarsMap.setLength(data.Nrows());
      ciftiExemplar.setMap(0, scalarsMap);
      CiftiFile outputFile;
      outputFile.setWritingFile(make_basename(filename)+extension+".nii");//sets up on-disk writing with default writing version
      outputFile.setCiftiXML(ciftiExemplar,false);
      vector<float> scratchRow(data.Nrows());//read/write a row at a time
      for (int64_t row=0;row<data.Ncols();row++) {
	for (int64_t col=0;col<data.Nrows();col++)
	  scratchRow[col]=data(col+1,row+1);
	outputFile.setRow(scratchRow.data(),row);
      }
    }
    else
      cerr << "GeneralSpatialAbstractor cannot save mode: " << sourceType << endl;
  }

  template class GeneralSpatialAbstractor<float>;

}
