include ${FSLCONFDIR}/default.mk

PROJNAME    = flame
XFILES      = flameo
USRINCFLAGS = `pkg-config CiftiLib --cflags`
LIBS        = -lfsl-newimage -lfsl-miscmaths  -lfsl-NewNifti \
              -lfsl-utils -lfsl-znz -lfsl-cprob `pkg-config CiftiLib --libs`

all: ${XFILES}

flameo: flameo.o design.o mcmc_mh.o gsoptions.o gsmanager.o gsa.o
	${CXX} ${CXXFLAGS} -o $@ $^ ${LDFLAGS}
