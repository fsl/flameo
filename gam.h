/*  Gam.h

    Mark Woolrich, Tim Behrens - FMRIB Image Analysis Group

    Copyright (C) 2002 University of Oxford  */

/*  CCOPYRIGHT  */

#if !defined(Gam_h)
#define Gam_h

#include <iostream>
#include <math.h>
#include "cprob/libprob.h"
#include "utils/tracer_plus.h"

using namespace Utilities;

namespace Gs {

  class Gam
    {
    public:
      static Gam& getInstance();
      ~Gam()
	{ delete gam;
	delete gamma;
	}

      void setParams(const double& pa, const double& pb) {
	b = pb;
	if(pa!=a)
	  {
	    a=pa;
	    delete gamma; gamma=new Gamma(a);
	  }
      }

      double pdf(const double& x);
      double neglog_pdf(const double& x);
      double cdf(const double& x);
      double rnd();

      double pdf(const double& pa, const double& pb, const double& x) {a = pa; b = pb; return pdf(x);}
      double neglog_pdf(const double& pa, const double& pb, const double& x) {a = pa; b = pb; return neglog_pdf(x);}
      double cdf(const double& pa, const double& pb, const double& x) {a = pa; b = pb; return cdf(x);}
      double rnd(const double& pa, const double& pb) {
       	setParams(pa,pb);
       	return rnd();
      }

    private:
      Gam():a(0.0),b(0.0),gamma(NULL)
	{}

      const Gam& operator=(Gam&);
      Gam(Gam&);

      static Gam* gam;

      double a;
      double b;
      Gamma* gamma;
    };

  inline Gam& Gam::getInstance(){
    if(gam == NULL)
      gam = new Gam();

    return *gam;
  }

  inline double Gam::rnd()
    {
      return gamma->Next()/b;
    }

  inline double Gam::pdf(const double& x)
    {
      if(x<=0)
	return 0;
      else
	return exp(a*log(b) + (a-1)*log(x) - b*x - lgam(a));
    }

  inline double Gam::neglog_pdf(const double& x)
    {
      if(x<=0)
	return 1e32;
      else
	return -(a*log(b) + (a-1)*log(x) - b*x - lgam(a));
    }
}

#endif
