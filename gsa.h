/*  Copyright (c) 2016 University of Oxford  */
/*  CCOPYRIGHT  */

#include "CiftiLib/CiftiFile.h"
#include "armawrap/newmat.h"
#include "newimage/newimageall.h"

namespace GSA {

  class uniqueNode {
  public:
  uniqueNode() : ID(0) {};
    int64_t ID;
    std::vector<int64_t> key;
  };

  template <class T>
  class AbstractGeometry {
  public:
  AbstractGeometry(std::vector<uniqueNode>& fullMap,const std::list<uniqueNode>& selectedNodes): dataMap(fullMap), myNodes(selectedNodes) {};
    virtual std::string type() {return "ABSTRACT";}
    int64_t nNodes() { return myNodes.size();}
  private:
    std::vector<uniqueNode>& dataMap;
    std::list<uniqueNode> myNodes;
  };

  template <class T>
  class CiftiSurfaceGeometry : AbstractGeometry<T> {
  public:
    CiftiSurfaceGeometry(const std::vector<cifti::CiftiBrainModelsMap::SurfaceMap>& inputMap) : dataMap(inputMap) {};
    virtual std::string type() {return "SURFACE";}
  private:
    std::vector<cifti::CiftiBrainModelsMap::SurfaceMap> dataMap;
  };

  template <class T>
  class VolumeGeometry : AbstractGeometry<T> {
  public:
  VolumeGeometry(const NEWIMAGE::volume<T> inputGeometry, const std::vector<uniqueNode>& fullMap,const std::vector<uniqueNode>& myMap): geometry(inputGeometry) {};
    NEWIMAGE::volume4D<T> asVolume(const NEWMAT::Matrix& data);
    NEWMAT::ReturnMatrix asMatrix(const NEWIMAGE::volume4D<T>& data);
    void updateMatrix(const NEWIMAGE::volume4D<T>& source, NEWMAT::Matrix& target);
    virtual std::string type() {return "VOLUME";}
    void maskMap(const NEWIMAGE::volume<T>& mask);
    NEWMAT::ReturnMatrix updateMapAndMatrix(const NEWIMAGE::volume<T>& mask, const NEWMAT::Matrix& data);
    NEWIMAGE::volume<T> geometry;
  };

template<class T>
class GeneralSpatialAbstractor
{
public:
  NEWMAT::ReturnMatrix abstractFile(const std::string&filename,const std::string& mode);
  void saveMatrix(const NEWMAT::Matrix& data, const std::string& filename);
  void remapData(NEWMAT::Matrix& data);
  std::vector<AbstractGeometry<T> > knownGeometries;
  std::vector<uniqueNode> storageMap;
private:
  std::string sourceType;
  cifti::CiftiXML ciftiExemplar;
  NEWIMAGE::volume<T> niftiGeometry;
};

}
